/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.inheritance;

/**
 *
 * @author Acer
 */
public class Duck extends Animal {

    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck created");
    }

    public void fly() {
        System.out.println("Duck: " + name + " fly!!");
    }

    @Override
    public void speak() {
        super.speak();
        System.out.println("Duck: " + name + " speak > gaab! gaab!!");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Duck: " + name + " walk with " + numberOfLegs + " legs.");
    }
}
