/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.inheritance;

/**
 *
 * @author Acer
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("ani", "white", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "White&Grey");
        dang.speak();
        dang.walk();
        Dog to = new Dog("To", "Orange");
        to.speak();
        to.walk();
        Dog mome = new Dog("Mome", "White&Black");
        mome.speak();
        mome.walk();
        Dog bat = new Dog("Bat", "White&Black");
        bat.speak();
        bat.walk();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();

        Duck som = new Duck("Som", "Yellow");
        som.speak();
        som.walk();
        som.fly();
        Duck gabgab = new Duck("GabGab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();

        System.out.println();
        Animal[] animals = {dang, to, mome, bat, zero, som, gabgab};
        
        for (int i = 0; i < animals.length; i++) {
            animals_Instanceof_Animal(animals, i);

            animals_Instanceof_AnimalType(animals, i);
            
            animals_Instanceof_Object(animals, i);
        }
        
        System.out.println();
        for (int i = 0; i < animals.length; i++) {
            animals[i].speak();
            animals[i].walk();
            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
        }

    }

    private static void animals_Instanceof_Object(Animal[] animals, int i) {
        System.out.println(animals[i].name + " is Object: " + (animals[i] instanceof Object));
    }

    private static void animals_Instanceof_AnimalType(Animal[] animals, int i) {
        if (animals[i] instanceof Dog) {
            Dog dog = (Dog) animals[i];
            System.out.println(dog.name + " is Dog: " + (dog instanceof Dog));
        } else if (animals[i] instanceof Cat) {
            Cat cat = (Cat) animals[i];
            System.out.println(cat.name + " is Cat: " + (cat instanceof Cat));
        } else if (animals[i] instanceof Duck) {
            Duck duck = (Duck) animals[i];
            System.out.println(duck.name + " is Duck: " + (duck instanceof Duck));
        }
    }

    private static void animals_Instanceof_Animal(Animal[] animals, int i) {
        System.out.println(animals[i].name + " is Animal: " + (animals[i] instanceof Animal));
    }
}
