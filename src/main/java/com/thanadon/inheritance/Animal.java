/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.inheritance;

/**
 *
 * @author Acer
 */
public class Animal {
    protected String name;
    protected String color;
    protected int numberOfLegs = 0;
    
    public Animal(String name, String color, int numberOfLegs){
        this.name = name;
        this.color = color;
        this.numberOfLegs = numberOfLegs;
        System.out.println("\nAnimal created");
    }
    
    public void speak(){
        System.out.println("Animal speak");
    }
    
    public void walk(){
        System.out.println("Animal walk");
    }
        
}
